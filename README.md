# Git Basics


***
Some screenshots of git bash executing some basic git commands are presented below. 
***
## Initializing Git repository & git checkout

![initial](./images/one.png)

## Git checkout and merge

![merge](./images/two.png)

## Git reset

![merge](./images/reset.png)

## Git diff (--staged)

![diff](./images/three.png)
![--staged](./images/four.png)

## Git clean

![clean](./images/clean.png)

## Git cherry-pick

![cherry-pick](./images/five.png)

### Adding remote origin and push the local repository to gitlab

![remote origin](./images/six.png)
![remote origin](./images/seven.png)